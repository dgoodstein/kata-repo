package anagramTest;
import anagram.AnagramSolver;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AnagramSolverTest {
	private AnagramSolver solver;
	@Before
	public void setUp() throws Exception {
		solver=new AnagramSolver("wordlist.txt");
		//solver=new AnagramSolver("ant.txt");
	}

	@Test
	public void testSetUp() {
		assertNotEquals(null,solver);
	}
	
	@Test
	public void readFileTest(){
		//solver.printCanonical();
		solver.print();
		assertEquals(20683,solver.getAnagramCount());
	}
	
	@Test
	public void findAnagramTest(){
		System.out.println(solver.findAnagram("mismate"));
		assertNotNull(solver.findAnagram("mismate"));
	}

}
