package supermarket;

import java.math.BigDecimal;

public class BulkDiscount extends Discount{
	int buy;
	BigDecimal price;
	
	public BulkDiscount(String id,int buy, BigDecimal price)
	{
		this.id=id;
		this.buy=buy;
		this.price=price;
	}
	
	public BigDecimal calculatePrice(double numberOfItems, BigDecimal originalPrice)
	{
		BigDecimal total=new BigDecimal("0");
		while (numberOfItems>=buy)
		{
			total=total.add(price);
			numberOfItems-=buy;
		}
		while (numberOfItems>0)
		{
			total=total.add(originalPrice);
			numberOfItems--;
		}
		return total;
	}

}
