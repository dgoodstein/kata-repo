package supermarket;

import java.io.FileNotFoundException;
public class DiscountReader extends CSVReader{
	public DiscountReader(String file) throws FileNotFoundException
	{
		super(file);
	}
	public Discount nextDiscount()
	{
		Discount tempDiscount=null;
		try
		{
			String id=in.next();
			String type=in.next();
			if (type.equals("bxgx"))
			{
				tempDiscount=new BxgxDiscount(id,in.nextInt(),in.nextInt());
			}
			else if (type.equals("bulk"))
			{
				tempDiscount=new BulkDiscount(id,in.nextInt(),in.nextBigDecimal());
			}
			return tempDiscount;
		}
		catch(Exception e)
		{
			System.out.println("invalid line for new Discount");
			return null;
		}
	}
}
