package supermarket;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CSVReader {
	Scanner in;
	public CSVReader(String file) throws FileNotFoundException
	{
	in=new Scanner(new File(file));	
	in.useDelimiter(",|\r\n");
	}
	public boolean hasNextLine()
	{
		return in.hasNextLine();
	}
	public int nextInt()
	{
		return in.nextInt();
	}
	public double nextDouble()
	{
		return in.nextDouble();
	}
	public String next()
	{
		return in.next();
	}
}
