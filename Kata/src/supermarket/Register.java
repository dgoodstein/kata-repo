package supermarket;

import java.math.BigDecimal;

public class Register {
	public static BigDecimal checkout (Store s, Cart c)
	{
		BigDecimal total=new BigDecimal(0.00);
		double numOfItems=0;
		for (String id:c.retrieveItems())
		{
			Item curItem=s.getItem(id);
			numOfItems=c.checkAmount(id);
			BigDecimal temp=(curItem.calculatePrice(numOfItems,curItem.getPrice()));
			System.out.println(id+"  "+temp.toString());
			total=total.add(temp);
		}
		System.out.println(total);
		return total;
	}
}
 