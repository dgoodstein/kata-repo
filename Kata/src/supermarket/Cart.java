package supermarket;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Cart {
	Map<String,Integer> inventory;
	public Cart()
	{
		inventory=new HashMap<String,Integer>();
	}
	public void add(String id, int amount)
	{
		inventory.put(id, amount);
	}
	public void remove(String id, int amount)
	{
		if (inventory.get(id)<=amount)
		{
			inventory.remove(id);
		}
		else
		{
			inventory.put(id,inventory.get(id)-amount);
		}
	}
	public void removeAll(String id)
	{
		inventory.remove(id);
	}
	public void empty()
	{
		inventory.clear();
	}
	public Set<String> retrieveItems()
	{
		return inventory.keySet();
	}
	public double checkAmount(String id)
	{
		return inventory.get(id);
	}
}
