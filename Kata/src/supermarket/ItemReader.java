package supermarket;

import java.io.FileNotFoundException;
public class ItemReader extends CSVReader{
	
	public ItemReader(String file) throws FileNotFoundException
	{
		super(file);
	}
	public Item nextItem()
	{
		try
		{
			Item tempItem=new Item(in.next(),in.next(),in.nextInt(),in.nextBigDecimal());
			return tempItem;
		}
		catch(Exception e)
		{
			System.out.println("invalid line for new Item");
			e.printStackTrace();
			return null;
		}
	}
}
