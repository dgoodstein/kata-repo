package supermarket;

import java.math.BigDecimal;

public class Item {
	String id;
	String name;
	int type;
	BigDecimal price;
	Discount discount;
	public Item(String id, String name, int type, BigDecimal price)
	{
		this.id=id;
		this.name=name;
		this.type=type;
		this.price=price;
		this.discount=new NullDiscount(id);
	}
	public void setDiscount(Discount d)
	{
		this.discount=d;
	}
	public String getId()
	{
		return id;
	}
	public String getName()
	{
		return name;
	}
	public int getType()
	{
		return type;
	}
	public BigDecimal getPrice()
	{
		return price;
	}
	public Discount getDiscount()
	{
		return discount;
	}
	public BigDecimal calculatePrice(double num, BigDecimal price)
	{
		return this.discount.calculatePrice(num, price);
	}
	
}
	
