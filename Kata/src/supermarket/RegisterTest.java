package supermarket;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class RegisterTest {
	Store s;
	Cart c;
	@Before
	public void setUp() throws Exception {
		String inventory="C:\\Users\\dgoodstein\\Documents\\BitBucket Repos\\Kata Repo\\Kata\\src\\supermarket\\inventory.csv";
		String itemFile="C:\\Users\\dgoodstein\\Documents\\BitBucket Repos\\Kata Repo\\Kata\\src\\supermarket\\itemList.csv";
		String discount="C:\\Users\\dgoodstein\\Documents\\BitBucket Repos\\Kata Repo\\Kata\\src\\supermarket\\discount.csv";
		s=new Store(itemFile,inventory,discount);
		c=new Cart();
		c.add("0001", 2);
		c.add("0002", 7);
		c.add("0003", 1);
		c.add("0004", 6);
	}

	@Test
	public void test() {
		BigDecimal total=Register.checkout(s,c);
		assertEquals(11.39,total.doubleValue(), 0.0001);
	}

}
