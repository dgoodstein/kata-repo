package supermarket;

import java.math.BigDecimal;

abstract class Discount {
	String id;
	abstract BigDecimal calculatePrice(double amountOfItems, BigDecimal originalPrice);
	
	public String getId()
	{
		return id;
	}
}
