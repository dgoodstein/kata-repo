package supermarket;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StoreTest {
	Store s;
	
	@Before
	public void setUp() throws Exception {
		String inventory="C:\\Users\\dgoodstein\\Documents\\BitBucket Repos\\Kata Repo\\Kata\\src\\supermarket\\inventory.csv";
		String itemFile="C:\\Users\\dgoodstein\\Documents\\BitBucket Repos\\Kata Repo\\Kata\\src\\supermarket\\itemList.csv";
		String discount="C:\\Users\\dgoodstein\\Documents\\BitBucket Repos\\Kata Repo\\Kata\\src\\supermarket\\discount.csv";
		s=new Store(itemFile,inventory,discount);
	}

	@Test
	public void badSetUp() throws Exception {
		String inventory="bad";
		String itemFile="bad";
		String discount="bad";
		s=new Store(itemFile,inventory,discount);
	}
	
	@Test
	public void testInventoryRead() {
		assertEquals("[1, 2, 3, 4]",s.inventoryKeys());
	}
	
	@Test
	public void testItemRead() {
		assertEquals("[1, 2, 3, 4]",s.itemKeys());
	}
	
	@Test
	public void testDiscountRead() {
		assertEquals("[2, 4]",s.discountKeys());
	}
	
	@Test
	public void testHasInStock() {
		assertTrue(s.hasInStock("0001"));
	}
	
	@Test
	public void testNotInStock() {
		assertFalse(s.hasInStock("0007"));
	}
	
	@Test
	public void testHasDiscount() {
		assertTrue(s.hasDiscount("0002"));
	}
	
	@Test
	public void testNoDiscount() {
		assertFalse(s.hasInStock("0007"));
	}
	
	@Test
	public void testGetStock() {
		assertEquals(20,s.getStock("0001"),0.0001);
	}
	
	@Test
	public void testPlaceItem() {
		double prevStock=s.getStock("0001");
		s.placeItem("0001",10);
		assertEquals(prevStock+10,s.getStock("0001"),0.0001);
	}
	
	@Test
	public void testPlaceMissingItem() {
		s.placeItem("0007",10);
		assertEquals(10,s.getStock("0007"),0.0001);
	}
	
	@Test
	public void testTakeItem() {
		double prevStock=s.getStock("0001");
		s.takeItem("0001",10);
		assertEquals(prevStock-10,s.getStock("0001"),0.0001);
	}
	
	@Test
	public void testTakeTooManyItem() {
		s.takeItem("0001",30);
		assertEquals(20,s.getStock("0001"),0.0001);
	}
	
	@Test
	public void testMissingItem() {
		assertEquals(0,s.takeItem("0007",1));
	}

}
