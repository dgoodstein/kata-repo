package supermarket;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CartTest {

	Cart c;
	@Before
	public void setUp() throws Exception {
		c=new Cart();
	}

	@Test
	public void testAdd() {
		c.add("0001",1);
		assertEquals("[1]", c.retrieveItems().toString());	
	}
	
	@Test
	public void testRemove() {
		c.add("0001",1);
		c.add("0003",2);
		c.remove("0001",1);
		assertEquals("[3]", c.retrieveItems().toString());	
	}
	
	@Test
	public void testRemovePartial() {
		c.add("0001",1);
		c.add("0003",2);
		c.remove("0003",1);
		assertEquals("[1, 3]", c.retrieveItems().toString());	
	}
	
	@Test
	public void testRemoveAll() {
		c.add("0001",1);
		c.add("0003",2);
		c.removeAll("0003");
		assertEquals("[1]", c.retrieveItems().toString());	
	}

	@Test
	public void testEmpty() {
		c.add("0001",1);
		c.add("0003",2);
		c.empty();
		assertEquals("[]", c.retrieveItems().toString());	
	}

}
