package supermarket;

import java.math.BigDecimal;

public class NullDiscount extends Discount{
	
 	public NullDiscount(String id)
	{
		this.id=id;
	}
	
	public BigDecimal calculatePrice(double numberOfItems, BigDecimal originalPrice)
	{
		BigDecimal total=new BigDecimal(numberOfItems);
		return total.multiply(originalPrice);
	}

}
