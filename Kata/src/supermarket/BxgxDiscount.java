package supermarket;

import java.math.BigDecimal;

public class BxgxDiscount extends Discount{
	int buy;
	int get;
	
	public BxgxDiscount(String id, int buy, int get)
	{
		this.id=id;
		this.buy=buy;
		this.get=get;
	}
	
	public BigDecimal calculatePrice(double numberOfItems, BigDecimal originalPrice)
	{
		BigDecimal total=new BigDecimal("0");
		while (numberOfItems>=buy+get)
		{
			total=total.add(new BigDecimal(buy).multiply(originalPrice));
			numberOfItems-=(buy+get);
		}
		while (numberOfItems>0)
		{
			total=total.add(originalPrice);
			numberOfItems--;
		}
		return total;
	}

}
