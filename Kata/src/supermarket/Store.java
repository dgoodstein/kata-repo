package supermarket;

import java.math.BigDecimal;
import java.util.HashMap;

public class Store {
	HashMap<String,Double> inventory;
	HashMap<String,Item> itemList;
	HashMap<String, Discount> discountList;
	
	public Store(String itemFile, String inventoryFile, String discountFile)
	{
		itemList=new HashMap<String,Item>();
		readItemsFromFile(itemFile);
		inventory=new HashMap<String,Double>();
		readInventoryFromFile(inventoryFile);
		readDiscountsFromFile(discountFile);
	}
	
	//Public Methods
	public String inventoryKeys()
	{
		return inventory.keySet().toString();
	}
	
	public String itemKeys()
	{
		return itemList.keySet().toString();
	}
	
	public String discountKeys()
	{
		return discountList.keySet().toString();
	}
	
	public boolean hasDiscount(String id)
	{
		return discountList.containsKey(id);
	}
	public Discount getDiscount(String id)
	{
		return discountList.get(id);
	}
	
	public BigDecimal getPrice(String id)
	{
		return itemList.get(id).getPrice();
	}
	
	public boolean hasInStock(String id)
	{
		return inventory.containsKey(id);
	}
	public Item getItem(String id)
	{
		return itemList.get(id);
	}
	
	public int takeItem(String id, int amount)
	{
		if (hasInStock(id))
		{
			double stock=getStock(id);
			if (stock<amount)
			{
				System.out.println("Not enough of "+id+" remaining.");
				return 0;
			}
			else
			{
				System.out.println("Added "+amount+" "+id+" to cart.");
				if (amount==stock)
				{
					inventory.remove(id);
				}
				else
				{
					inventory.put(id, (double)stock-amount);
				}
				return amount;
			}
		}
		else
		{
			System.out.println ("Item not in stock.");
			return 0;
		}
	}
	
	public int placeItem(String id, int amount)
	{
		if (hasInStock(id))
		{
			inventory.put(id,inventory.get(id)+amount);
		}
		else
		{
			inventory.put(id, (double)amount);
		}
		return amount;
	}
	
	public double getStock(String id)
	{
		if (inventory.containsKey(id))
		{
			return inventory.get(id);
		}
		else
		{
			return 0;
		}
	}
	//Private methods
	
	private void readItemsFromFile(String itemFile)
	{
		Item tempItem;
		try
		{
			System.out.println(itemFile);
			ItemReader reader=new ItemReader(itemFile);
			while (reader.hasNextLine())
			{
				tempItem=reader.nextItem();
				itemList.put(tempItem.getId(), tempItem);
			}
		}
		catch(Exception e)
		{
			System.out.println ("Item File not found");
		}
	}
	private void readInventoryFromFile(String inventoryFile)
	{
		try
		{
			CSVReader reader=new CSVReader(inventoryFile);
			while (reader.hasNextLine())
			{
				inventory.put(reader.next(),reader.nextDouble());
			}
		}
		catch(Exception e)
		{
			System.out.println ("Inventory File not found");
		}
	}
	private void readDiscountsFromFile(String discountFile)
	{
		Discount tempDiscount;
		try
		{
			DiscountReader reader=new DiscountReader(discountFile);
			while (reader.hasNextLine())
			{
				tempDiscount=reader.nextDiscount();
				Item curItem=itemList.get(tempDiscount.getId());
				curItem.setDiscount(tempDiscount);
				itemList.put(tempDiscount.getId(), curItem);
			}
		}
		catch(Exception e)
		{
			System.out.println ("Discount File not found");
		}
	}
}
