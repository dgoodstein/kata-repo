package anagram;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class AnagramSolver {
	private Scanner in;
	private Map<String, ArrayList<String>> wordStorage;
	//private enum caseSensitive {ON, OFF}//Wanted to try using, but couldn't actually figure out yet
	
	public AnagramSolver(String file) throws FileNotFoundException
	{
		in=new Scanner(new File(file));
		wordStorage=new HashMap<String, ArrayList<String>>();
		processFile();
		in.close();
	}
	/**
	 * @return How many sets of anagrams are included in the file
	 */
	public int getAnagramCount()
	{
		return wordStorage.size();
	}
	/**
	 * Prints the canonical keys for the list of anagrams
	 */
	public void printCanonical()
	{
		System.out.println(wordStorage.keySet().toString());
	}
	/**
	 * Prints the entire list of anagrams
	 */
	public void print()
	{
		for (String key:wordStorage.keySet())
		{
			System.out.println(key+" "+wordStorage.get(key).toString());
		}
	}
	/**
	 * @param word The search term
	 * @return all anagrams of the word contained in the file
	 */
	public String findAnagram(String word)
	{
		char[] wordArray=word.toCharArray();
		Arrays.sort(wordArray);
		String key=new String(wordArray);
		return wordStorage.get(key).toString();
	}
	
	/**
	 * @return the next line in a text file, or an empty string if it fails.
	 */
	private String readNext()
	{
		if (in.hasNextLine())
		{
			return in.nextLine();
		}
		else
		{
			return "";
		}
	}
	/**
	 * @param s is the original string
	 * @return the canonical form of the string
	 */
	private String prepare(String s)
	{
		s=s.trim();
		char[] stringSplit=s.toCharArray();
		Arrays.sort(stringSplit);
		return new String(stringSplit);
	}
	private void processFile()
	{
		Map<String,String> tempMap=new HashMap<String, String>();
		String curLine;
		String canonical;
		ArrayList<String> arr;
		do
		{
			curLine=readNext();
			canonical=prepare(curLine);
			if (wordStorage.containsKey(canonical))
			{
				arr=wordStorage.get(canonical);
				arr.add(curLine);
				wordStorage.put(canonical,arr);
			}
			else if (tempMap.containsKey(canonical))
			{
				arr=new ArrayList<String>();
				arr.add(tempMap.get(canonical));
				arr.add(curLine);
				wordStorage.put(canonical, arr);
			}
			else
			{
				tempMap.put(canonical, curLine);
				
			}
		} while(curLine.length()>0);
	}
}
