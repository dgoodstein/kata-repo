package triangles;

import java.util.Scanner;

public class TrianglesKata {

	public static void main(String[] args) {
		int size;
		Scanner in;
		System.out.println("Pick a size");
		in=new Scanner(System.in);
		size=in.nextInt();
		Triangle t=new Triangle(size);
		System.out.println(t.displayString());
		in.close();
	}
}