package triangles;

import static org.junit.Assert.*;

import org.junit.Test;

public class TrianglesKataTest {

	@Test
	public void testSize1() {
		Triangle t=new Triangle(1);
		assertEquals(t.displayString(),"*\n");
	}
	
	@Test
	public void testSize5() {
		Triangle t=new Triangle(5);
		assertEquals(t.displayString(),"        *        \n"
									+  "      * * *      \n"
									+  "    * * * * *    \n"
									+  "  * * * * * * *  \n"
									+  "* * * * * * * * *\n");
	}
	
	@Test
	public void testSize7() {
		Triangle t=new Triangle(7);
		assertEquals(t.displayString(),"            *            \n"
									+  "          * * *          \n"
									+  "        * * * * *        \n"
									+  "      * * * * * * *      \n"
									+  "    * * * * * * * * *    \n"
									+  "  * * * * * * * * * * *  \n"
									+  "* * * * * * * * * * * * *\n");
	}

}
