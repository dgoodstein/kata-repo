package triangles;

public class Triangle {
	int size;
	
	public Triangle(int inputSize)
	{
		size=inputSize;
	}
	
	public String displayString()
	{
		String displayString="";
		for (int index=0;index<size;index++) //From 1 to X
		{
			for (int innerIndex=0;innerIndex<size-(index+1);innerIndex++) //0 to 4 at top if 5, 0 to 0  at bottom
			{
				displayString+="  ";
			}
			for (int innerIndex=0;innerIndex<index*2;innerIndex++)
			{
				displayString+="* ";
			}
			displayString+="*";
			for (int innerIndex=0;innerIndex<size-(index+1);innerIndex++) 
			{
				displayString+="  ";
			}
			displayString+="\n";
		}
		return displayString;
	}
}
