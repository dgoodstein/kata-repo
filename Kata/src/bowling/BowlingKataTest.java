package bowling;

import static org.junit.Assert.*;

import org.junit.Test;

public class BowlingKataTest {
	BowlingKata b;
	int[] rolls;
	
	
	
	@Test
	public void TestZeroGame() throws Exception {
		rolls=new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		b=new BowlingKata(rolls);
		assertEquals(b.score(),0);
	}
	
	@Test
	public void TestOnePointGame() throws Exception {
		rolls=new int[]{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		b=new BowlingKata(rolls);
		assertEquals(b.score(),1);
	}
	
	@Test
	public void TestStrike() throws Exception {
		rolls=new int[]{10,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		b=new BowlingKata(rolls);
		assertEquals(b.score(),18);
	}
	
	@Test
	public void TestPerfectGame() throws Exception {
		rolls=new int[]{10,10,10,10,10,10,10,10,10,10,10,10};
		b=new BowlingKata(rolls);
		assertEquals(b.score(),300);
	}

	@Test
	public void TestSpare() throws Exception {
		rolls=new int[]{5,5,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		b=new BowlingKata(rolls);
		assertEquals(b.score(),16);
	}
	
	@Test
	public void TestAndrewNumbers() throws Exception {
		rolls=new int[]{7, 0, 7, 3, 6, 3, 9, 0, 10, 8, 1, 9, 1, 10, 7, 2, 8, 2, 7};
		b=new BowlingKata(rolls);
		assertEquals(b.score(),134);
	}
	
}
