package bowling;

public class BowlingKata 
{
	int[] rolls;
	int score;
	public BowlingKata(int[] inputRolls)
	{
		rolls=inputRolls;
		
	}
	public int score()
	{
		score=0;
		int rollIndex=0;
		for (int frame=0;frame<10;frame++)
		{
			if (isStrike(rollIndex))
			{
				score+=10;
				score+=(rolls[rollIndex+1]+rolls[rollIndex+2]);
				rollIndex++;
			}
			else if (isSpare(rollIndex))
			{
				score+=10;
				score+=(rolls[rollIndex+2]);
				rollIndex+=2;
			}
			else
			{
				score+=(rolls[rollIndex]+rolls[rollIndex+1]);
				rollIndex+=2;
			}
		}
		return score;
	}
	private boolean isStrike(int index)
	{
		return rolls[index]==10;
	}
	private boolean isSpare(int index)
	{
		return rolls[index]+rolls[index+1]==10;
	}
}


